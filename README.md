# README #

JobsApp with mail actions. It was created using cakePHP 3.*!

# Short instructions #
1. Clone
2. composer update
3. To use SQLite database
	rename config\app.default.php to config\app.php
4. Set your Smtp settings for sending mails.
5. Check settings for sqlite in php.ini file
