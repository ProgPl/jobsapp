<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;
use Cake\Routing\Router;

/**
 * Job mailer.
 */
class JobMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    //static public $name = 'Job';

    public function addNewJob($job, $mailActionsData)
    {
    	$this->to($mailActionsData['email']);

        if (!empty($mailActionsData['cemail'])) {
        	$this->addTo($mailActionsData['cemail']);	
        }
        
        $actions = array();
        $actions['view'] = Router::url('/', true)."jobs/view/".$job->id;
        $actions['edit'] = Router::url('/', true)."jobs/edit/".$job->id;
        $this->subject(sprintf('Added new job "%s"', $job->title));
    	if (!empty($mailActionsData['actions'])) {
            foreach ($mailActionsData['actions'] as $action => $actionData) {
            	$actions[$action] = Router::url('/', true)."?token=".$actionData[$action.'_token'];
            }
        }
        $this->set(['actions' => $actions]);
        $this->set(['www' => Router::url('/', true)]);
        $this->set(['job_title' => $job->title]);
        $this->set(['name' => $mailActionsData['name']]);

        $this->emailFormat('html');
        $this->template('addNewJob');
    }
}
