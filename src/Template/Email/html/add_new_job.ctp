<h1>Hello <?= $name ?>,</h1>
<p>You create new job "<?= $job_title ?>" on site <a href="<?= $www ?>"><?= $www ?></a></p>
<p><u>You can view, edit, activate, deactevate, delete your job using links below:</u></p>
<ol>
<?php foreach ($actions as $action => $actionUrl) { ?>
	<li><a href="<?= $actionUrl ?>"><?= $action ?></a></li>
<?php } ?>
</ol>

<p>
	Best regards,<br>
	Administration<br>
	<a href="<?= $www ?>">www</a>
</p>