<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use App\Helpers\MailActionsHelpers as MailActionsH;
use App\Model\Table\JobsTable;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param string ...$path Path segments.
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        // Handle request with token
        if (!empty($this->request->query['token'])) {
            $token = $this->request->query['token'];
            $this->loadModel('Jobs');
            $job = $this->Jobs->get(MailActionsH::getJobIdByToken($token), [
                'contain' => []
            ]);
            
            $result = MailActionsH::handleTokenAction($token, $job);
            if (!$result['success']) {
                $this->Flash->error(__($result['message']));
            } else {
                if ($result['action'] == 'delete') {
                   if ($this->Jobs->delete($job)) {
                        $this->Flash->success(__($result['message']));
                    } else {
                        $this->Flash->error(__('The job could not be deleted. Please, try again.'));
                    }
                } else {
                    if ($this->Jobs->save($job)) {
                        $this->Flash->success(__($result['message']));
                    } else {
                        $this->Flash->error(__('The job could not be saved. Please, try again.'));
                    }
                }
            }
        }

        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
