<?php

namespace App\Helpers;

use Cake\Mailer\Email;

class MailActionsHelpers
{

	private static $actionList = array('activate', 'deactivate', 'delete');

	private static function createToken($action, $job, $actionCode)
	{
		$str = 'mail_action='.$action.'&details='.$job->id.';'.$job->user_id.';'.$actionCode;
		return sha1($str);
	}

	public static function getJobIdByToken($token)
	{
		$token = trim($token);
		$idLenght = substr($token, 0, 1);
        $jobId = substr($token, 1, $idLenght);

        return $jobId;
	}

	public static function getActionByToken($token, $action_codes)
	{
		$action = "";
		$actionCodes = explode(":", $action_codes);
		
		foreach ($actionCodes as $key => $codeStr) {
			$code = explode("=", $codeStr);
			
			$jobAction = $code[0];
			$codeStr = $code[1];
			$code = explode(";", $codeStr);
			$jobToken = $code[0];
			if ($token == $jobToken) {
				$action = $jobAction;
				break;
			}

		}

		return $action;
	}

	public static function handleTokenAction($token, $job)
	{
		$action = self::getActionByToken($token, $job->action_codes);
		switch ($action) {
			case 'activate':
				$job->set('active', 1);
				$message = "The Job '" . $job->title . "' has been activated!";
				$result['success'] = 1;
				break;
			case 'deactivate':
				$job->set('active', 0);
				$message = "The Job '" . $job->title . "' has been deactivated!";
				$result['success'] = 1;
				break;
			case 'delete':
				$message = "The Job '" . $job->title . "''  has been deleted!";
				$result['success'] = 1;
				break;
			default:
				$message = "Unknown token!";
				$result['success'] = 0;
				break;
		}
		
		$result['action'] = $action;
		$result['job'] = $job;
		$result['message'] = $message;
		
		return $result;
	}

    public static function getMailActionData($job, $user)
    {
    	
		if(empty($user->emails) AND empty($job->action_emails)) {
			return false;
		}
		
		$result = array(
    		'email' => $user->email, 
    		'name' => $user->name,
    		'cemail' => (!empty($job->action_emails)? $job->action_emails: ""), 
    		'actions' => array(),
        );
        
        foreach(self::$actionList as $action) {
        	$actionCode = m_randomToken();
        	$actionToken = strlen($job->id).$job->id.self::createToken($action, $job, $actionCode);
            
            $result['actions'][$action][$action.'_token'] = $actionToken;
            $result['actions'][$action][$action.'_code'] = $actionCode;
        }
        
        return $result;
    }
}
